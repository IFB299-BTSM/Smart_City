from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic import TemplateView
from search_page.models import Place, Event

def add_variable_to_context(request):
	querysetPlace = Place.objects.all().order_by("type")
	querysetEvent = Event.objects.all()


	return {
        'testme': 'Hello world!',
        'places_list' : querysetPlace,
        'events_list' : querysetEvent
    }