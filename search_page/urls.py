from django.conf.urls import url
from django.contrib import admin
from .views import search


urlpatterns = [
    url(r'^$', search),
]