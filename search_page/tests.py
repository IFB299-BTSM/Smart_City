from django.test import TestCase, Client
import datetime
from django.utils import timezone
from django.core.urlresolvers import reverse
from .models import Place, Event
from .views import search
from create_event.forms import EventForm


class placeModelTests(TestCase):

	def setUp(self):
		Place.objects.create(title="QUT", address="2 George Street", description="A university for the real world", type="College", picture='picture/QUT-picture.png')

	def test_place_name(self):
		"""Place name is correctly saved to database"""
		qut = Place.objects.get(title="QUT")
		self.assertEqual(qut.title, "QUT")

	def test_place_address(self):
		"""Place address is correctly saved to database"""
		qut = Place.objects.get(title="QUT")
		self.assertEqual(qut.address, "2 George Street")
	
	def test_place_description(self):
		"""Place description is correctly saved to database"""
		qut = Place.objects.get(title="QUT")
		self.assertEqual(qut.description, "A university for the real world")

	def test_place_type(self):
		"""Place type is correctly saved to database"""
		qut = Place.objects.get(title="QUT")
		self.assertEqual(qut.type, "College")

	def test_place_lat(self):
		"""Place latitude is correctly saved to database"""
		qut = Place.objects.get(title="QUT")
		self.assertEqual(qut.latitude, 1.0)

	def test_place_long(self):
		"""Place longitude is correctly saved to database"""
		qut = Place.objects.get(title="QUT")
		self.assertEqual(qut.longitude, 1.0)

	def test_search_page_load(self):
		"""Search page correctly loads on url"""
		client = Client()
		response = self.client.get('http://127.0.0.1:8000/search/')
		self.assertEqual(response.status_code, 200)

	def test_place_page_load(self):
		"""Place page correctly loads on url"""
		client = Client()
		response = self.client.get('http://127.0.0.1:8000/places/')
		self.assertEqual(response.status_code, 200)

	def test_place_save(self):
		"""Place saves correctly"""
		client = Client()
		place = Place(title="Museum123", address="3 George Street", description="A test place", type="Museum")
		place.save()
		self.assertEqual(place.title, "Museum123")

	def test_place_picture(self):
		"""Place picture is correctly saved to database"""
		qut = Place.objects.get(title="QUT")
		self.assertEqual(qut.picture, "picture/QUT-picture.png")

class eventModelTests(TestCase):

	def setUp(self):
		time_now = timezone.now()
		Event.objects.create(title="Coldplay Live", address="59 Gardens Point Rd", description="Coldplay live at Riverstage", type="Concert", event_time=time_now, picture='picture/coldplay.jpg')

	def test_event_name(self):
		"""Event name is correctly saved to database"""
		coldplay = Event.objects.get(title="Coldplay Live")
		self.assertEqual(coldplay.title, "Coldplay Live")

	def test_event_address(self):
		"""Event address is correctly saved to database"""
		coldplay = Event.objects.get(title="Coldplay Live")
		self.assertEqual(coldplay.address, "59 Gardens Point Rd")

	def test_event_description(self):
		"""Event description is correctly saved to database"""
		coldplay = Event.objects.get(title="Coldplay Live")
		self.assertEqual(coldplay.description, "Coldplay live at Riverstage")

	def test_event_type(self):
		"""Event type is correctly saved to database"""
		coldplay = Event.objects.get(title="Coldplay Live")
		self.assertEqual(coldplay.type, "Concert")

	def test_event_save(self):
		"""Event saves correctly"""
		client = Client()
		event = Event(title="Concert123", address="59 Gardens Point Rd", description="Coldplay live at Riverstage", type="Concert", event_time="2012-09-18 12:30")
		event.save()
		self.assertEqual(event.title, "Concert123")

	def test_event_page_load(self):
		"""Event page correctly loads on url"""
		client = Client()
		response = self.client.get('http://127.0.0.1:8000/events/')
		self.assertEqual(response.status_code, 200)

	def test_event_forms_valid(self):
		"""Correct form data makes valid form"""
		form_data = {
		'title': 'test form',
		'address': '123 test street',
		'description': 'a test event form',
		'type': 'Concert',
		'picture': 'testimage.png',
		'event_time': '10/25/2006 14:30'

		}
		form = EventForm(data=form_data)
		self.assertTrue(form.is_valid())

	def test_event_forms_invalid_type(self):
		"""Incorrect form data makes invalid form - type"""
		form_data = {
		'title': 'test form',
		'address': '123 test street',
		'description': 'a wrong test event form',
		'type': 'test',
		'picture': 'testimage.png',
		'event_time': '10/25/2006 14:30'

		}
		form = EventForm(data=form_data)
		self.assertFalse(form.is_valid())

	def test_event_forms_invalid_time(self):
		"""Incorrect form data makes invalid form - time"""
		form_data = {
		'title': 'test form',
		'address': '123 test street',
		'description': 'a wrong test event form',
		'type': 'Concert',
		'picture': 'testimage.png',
		'event_time': '1235555'

		}
		form = EventForm(data=form_data)
		self.assertFalse(form.is_valid())

	def test_event_forms_invalid_max_char(self):
		"""Incorrect form data makes invalid form - max char"""
		form_data = {
		'title': 'test form test form test form test form test form test form test form test form test form test form test form test form test form test form test form test form ',
		'address': '123 test street',
		'description': 'a wrong test event form',
		'type': 'Concert',
		'picture': 'testimage.png',
		'event_time': '10/25/2006 14:30'

		}
		form = EventForm(data=form_data)
		self.assertFalse(form.is_valid())

	def test_event_picture(self):
		"""Event picture is correctly saved to database"""
		coldplay = Event.objects.get(title="Coldplay Live")
		self.assertEqual(coldplay.picture, "picture/coldplay.jpg")