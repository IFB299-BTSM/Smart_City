from __future__ import unicode_literals
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import TemplateView
from django.contrib.auth.forms import UserCreationForm
from .models import Place, Event
from django.db.models import Q

# Create your views here.
def search(request):
	querysetPlace = Place.objects.all().order_by("type")
	querysetEvent = Event.objects.all()
	query = request.GET.get("q")

	#have a base list with everything, remove whats ticked from that list,
	#then exclude that list (the remaining is whats not ticked)

	title = "Search Something!"

	col_check = lib_check = ind_check = hot_check = par_check = zoo_check = mus_check = res_check = mal_check = ""

	place_checklist = [col_check, lib_check, ind_check, hot_check, par_check, zoo_check, mus_check, res_check, mal_check]

	place_types = ['College', 'Library', 'Industry', 'Hotel', 
		'Park', 'Zoo', 'Museum', 'Restaurant', 'Mall']

	checkbox_set = request.GET.getlist('place')

	if query == "" or query == None:
		checkbox_set = ['College', 'Library', 'Industry', 'Hotel', 
		'Park', 'Zoo', 'Museum', 'Restaurant', 'Mall']

	#saving checkbox values

	# I apologise

	if 'College' in checkbox_set:
		col_check = 'checked'
	else:
		col_check = ''	

	if 'Library' in checkbox_set:
		lib_check = 'checked'
	else:
		lib_check = ''

	if 'Industry' in checkbox_set:
		ind_check = 'checked'
	else:
		ind_check = ''

	if 'Hotel' in checkbox_set:
		hot_check = 'checked'
	else:
		hot_check = ''

	if 'Park' in checkbox_set:
		par_check = 'checked'
	else:
		par_check = ''

	if 'Zoo' in checkbox_set:
		zoo_check = 'checked'
	else:
		zoo_check = ''

	if 'Museum' in checkbox_set:
		mus_check = 'checked'
	else:
		mus_check = ''

	if 'Restaurant' in checkbox_set:
		res_check = 'checked'
	else:
		res_check = ''

	if 'Mall' in checkbox_set:
		mal_check = 'checked'
	else:
		mal_check = ''

	for i in checkbox_set:
		place_types.remove(i)

	#test = "excluding: " + str(place_types)

	if query:
		querysetPlace = querysetPlace.filter(

			Q(title__icontains=query) |
			Q(address__icontains=query) |
			Q(description__icontains=query) | 
			Q(type__icontains=query) 
			).distinct()

		for types in place_types:
			querysetPlace = querysetPlace.exclude(type=types)

		title = 'Search Results:'


		querysetEvent = querysetEvent.filter(
			Q(title__icontains=query) |
			Q(address__icontains=query) |
			Q(description__icontains=query) |
			Q(type__icontains=query)
			).distinct()

		if len(querysetEvent) == 0 and len(querysetPlace) == 0:
			title = 'No places or events found'

	context = {
		"events_list": querysetEvent,
		"places_list": querysetPlace,
		"title": title,
		"col_check": col_check,
		"lib_check": lib_check,
		"ind_check": ind_check,
		"hot_check": hot_check,
		"par_check": par_check,
		"zoo_check": zoo_check,
		"mus_check": mus_check,
		"res_check": res_check,
		"mal_check": mal_check

	}
	return render(request, "search_page.html", context)

