# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from register_page.admin import UserCreationForm
from register_page.models import *
from django.test import TestCase
from django import forms
from django.core.files import File

# Create your tests here.
class RegisterPageTests(TestCase):

    def setup(self):
        pass


    #Valid Form Data
    def test_UserCreationForm_valid(self):
        image = File(open("/Users/kylethacker/PycharmProjects/Smart_City/static/images/logo.png", 'rb'))

        form = UserCreationForm(
            data={'username': "kyle", 'email': "test@test.com", 'type': "Student", 'password1': "Gillhespy1!",
                      "password2": 'Gillhespy1!', 'family_name': "Thacker", 'given_names': "Kyle Daniel"}, files= {'profile_picture': image})
        print(form.errors)
        self.assertTrue(form.is_valid())


    # Invalid Form Data
    def test_UserCreationForm_invalid(self):
        form = UserCreationForm(data={'email': "", 'password': "mp", 'first_name': "mp", 'phone': ""})
        self.assertFalse(form.is_valid())

    #Equal username
    def test_UserProfileModel_username(self):

        test = UserProfile(username="kyle")
        self.assertEqual("kyle", test.username)

    # Equal given_names
    def test_UserProfileModel_givennames(self):
        test = UserProfile(given_names="kyle")
        self.assertEqual("kyle", test.given_names)

     # Equal family_names
    def test_UserProfileModel_familynames(self):
        test = UserProfile(family_name="kyle")
        self.assertEqual("kyle", test.family_name)

    # Equal types
    def test_UserProfileModel_types(self):
        test = UserProfile(type="Student")
        self.assertEqual("Student", test.type)

    #create user in UserProfile
    def test_UserProfile_Create(self):
        image = File(open("static/images/logo.png", 'rb'))


        up = UserProfileManager.create_user(UserProfile.objects, username="kylet", email="test@test.com", given_names="kyle", family_name="thacker", type="Student", password='p', profile_picture=image)
        self.assertTrue(isinstance(up, UserProfile))

    #Missing username for Userprofile
    def test_Username_Incorrect(self):
        with self.assertRaises(TypeError):

            image = File(
                open("static/images/logo.png", 'rb'))

            up = UserProfileManager.create_user(UserProfile.objects, email="test@test.com",
                                                given_names="kyle", family_name="thacker", type="Student", password='p',
                                                profile_picture=image)

    #Invalid password criteria
    def test_PassToShort(self):

        image = File(open("static/images/logo.png", 'rb'))
        form = UserCreationForm(
                data={'username': "kyle", 'email': "test@test.com", 'type': "Student", 'password1': "g",
                      "password2": "g", 'family_name': "Thacker", 'given_names': "Kyle Daniel"},
                files={'profile_picture': image})

        self.assertFalse(form.is_valid())

    # Invalid password criteria

    def test_PassOnlyNumbers(self):
        image = File(open("static/images/logo.png", 'rb'))
        form = UserCreationForm(
            data={'username': "kyle", 'email': "test@test.com", 'type': "Student", 'password1': "111111111",
                  "password2": "111111111", 'family_name': "Thacker", 'given_names': "Kyle Daniel"},
            files={'profile_picture': image})
        self.assertFalse(form.is_valid())

    def test_PassNoSepcial(self):
        image = File(open("static/images/logo.png", 'rb'))
        form = UserCreationForm(
                data={'username': "kyle", 'email': "test@test.com", 'type': "Student", 'password1': "g1g1g1g1g1",
                      "password2": "g1g1g1g1g1", 'family_name': "Thacker", 'given_names': "Kyle Daniel"},
                files={'profile_picture': image})
        self.assertFalse(form.is_valid())

    def test_PassNoMatch(self):
        image = File(open("static/images/logo.png", 'rb'))
        form = UserCreationForm(
            data={'username': "kyle", 'email': "test@test.com", 'type': "Student", 'password1': "g1g1g12g1g1!",
                  "password2": "g1g1g1g1g1!", 'family_name': "Thacker", 'given_names': "Kyle Daniel"},
            files={'profile_picture': image})


        self.assertFalse(form.is_valid())


    def test_SubmitForm(self):
        pass

    #Registerpage view loads
    def test_LoadRegisterPage(self):
        response = self.client.get('http://127.0.0.1:8000/register/')

        self.assertEqual(response.status_code, 200)

     #RegisterPage contains form elements
    def test_LoadRegisterPageHasUsernameField(self):
        response = self.client.get('http://127.0.0.1:8000/register/')


        self.assertContains(response, "username")

    def test_LoadRegisterPageHasEmailField(self):
        response = self.client.get('http://127.0.0.1:8000/register/')


        self.assertContains(response, "email")

    def test_LoadRegisterPageHasTypeField(self):
        response = self.client.get('http://127.0.0.1:8000/register/')


        self.assertContains(response, "type")

    def test_LoadRegisterPageHasPassword1Field(self):
        response = self.client.get('http://127.0.0.1:8000/register/')


        self.assertContains(response, "password1")

    def test_LoadRegisterPageHasPassword2Field(self):
        response = self.client.get('http://127.0.0.1:8000/register/')

        self.assertContains(response, "password2")

    def test_LoadRegisterPageHasGivenNamesField(self):
        response = self.client.get('http://127.0.0.1:8000/register/')

        self.assertContains(response, "given_names")

    def test_LoadRegisterPageHasFamilyNameField(self):
        response = self.client.get('http://127.0.0.1:8000/register/')

        self.assertContains(response, "family_name")

    def test_LoadRegisterPageHasProfilePictureField(self):
        response = self.client.get('http://127.0.0.1:8000/register/')

        self.assertContains(response, "profile_picture")





