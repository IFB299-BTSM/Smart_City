# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.contrib.auth.forms import UserCreationForm
from register_page.forms import UserForm
from django.http import HttpResponse

# Create your views here.


def register(request):
    if request.method == 'POST':
        form = UserForm(request.POST, request.FILES)

        if form.is_valid():

            form.save()

            return redirect('/login')

    else:
        form = UserForm()

    args = {'form': form}
    html = render(request, 'register_page.html', args)
    return HttpResponse(html)

