
from django import forms

from django.contrib.auth.models import User
from register_page.models import UserProfile
from register_page.admin import UserCreationForm




class UserForm(UserCreationForm):


    class Meta:
        model = UserProfile
        fields = ('username', 'email', 'type', 'password1', 'password2', 'profile_picture')






    def save(self, commit=True):
        user = super(UserForm, self).save(commit=False)
        if commit:
            user.save()


