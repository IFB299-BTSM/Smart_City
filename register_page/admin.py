# -*- coding: utf-8 -*-
from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.core.files.images import get_image_dimensions
import re
from register_page.models import UserProfile
from django.utils.safestring import mark_safe


# Register your models here.

class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""

    username        = forms.CharField(max_length = 120, required = True)
    password1       = forms.CharField(label='Password', widget=forms.PasswordInput, help_text="<p>"
                                                                                              "&#9679; Password must be at least 8 characters long</li>"
                                                                                              "<br>&#9679; Password must be a mixture of letters/numbers/special characters</li>"
                                                                                              "</p>")

    password2       = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)
    given_names     = forms.CharField(max_length = 120)
    family_name     = forms.CharField(max_length = 120)
    profile_picture = forms.ImageField()


    choices = (('',''),('Student', 'Student'), ('Businessman', 'Businessman'), ('Tourist', 'Tourist'))
    type = forms.ChoiceField(choices, required=True)



    class Meta:
        model = UserProfile
        fields = ('username','email', 'type', 'given_names', 'family_name', 'profile_picture')

    def clean_password1(self):
        error = False
        errors = []
        password1 = self.cleaned_data.get("password1")
        if len(password1) <8:
            errors.append(forms.ValidationError(mark_safe("<p>&#10071; Password must be over 8 characters!</p>")))
            error = True
        if password1.isnumeric():
            errors.append(forms.ValidationError(mark_safe("<p>&#10071; Password must not be all numbers!</p>")))
            error = True
        if re.match("^[A-Za-z0-9]+$", password1):
            errors.append(forms.ValidationError(mark_safe("<p>&#10071; Password must contain special characters! (e.g. !@#$%^&*())</p>")))
            error = True

        if error == False:
            return password1
        else:
                raise forms.ValidationError(errors)





    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2






    def clean_email(self):
        email = self.cleaned_data.get("email")

        return email

    def clean_given_names(self):
         given_names = self.cleaned_data.get("given_names")

         return given_names

    def clean_family_name(self):
         family_name = self.cleaned_data.get('family_name')

         return family_name

    def clean_username(self):
        username = self.cleaned_data.get("username")
        if username and UserProfile.objects.filter(username=username).exists():
            raise forms.ValidationError('Username must be unique.')
        return username

    def clean_profile_picture(self):
        profile_picture = self.cleaned_data['profile_picture']
        try:
            width, height = get_image_dimensions(profile_picture)

            #validate dimensions
            max_height = max_width = 1000
            if width > max_width or height > max_height:
                raise forms.ValidationError('Please use an image that is '
                     '%s x %s pixels or smaller.' % (max_width, max_height))

            #validate file type
            main, sub = profile_picture.content_type.split('/')
            if not (main == 'image' and sub in ['gif', 'jpeg', 'png']):
                raise forms.ValidationError('Please use a GIF, JPEG or PNG image.')

            #validate file size
            if len(profile_picture) > (30 * 1024):
                raise forms.ValidationError('Please use an image that is less than 20k.')

        except AttributeError:
            "Handles case of updating profile without providing a profile picture"
            pass
        return profile_picture





    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.type = (self.cleaned_data['type'])
        user.username = (self.cleaned_data['username'])
        user.email = (self.cleaned_data['email'])
        user.given_names = (self.cleaned_data['given_names'])
        user.family_name = (self.cleaned_data['family_name'])
        user.profile_picture = (self.cleaned_data['profile_picture'])


        if commit:
            user.save()
        return user



class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField(label= ("Password"),
        help_text= ("Change your password using <a href=\"change_password/\">this form</a>."))


    class Meta:
        model = UserProfile
        fields = ('username', 'email', 'type', 'given_names', 'family_name', 'password', 'profile_picture', 'is_active', 'is_admin')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('username','email', 'type', 'is_admin', 'profile_picture')
    list_filter = ('is_admin',)
    fieldsets = (
        (None, {'fields': ('username','email', 'password', 'profile_picture')}),
        ('Personal info', {'fields': ('type', 'given_names', 'family_name')}),
        ('Permissions', {'fields': ('is_admin',)}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username','email', 'type', 'given_names', 'family_name', 'profile_picture', 'password1', 'password2')}
        ),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()

# Now register the new UserAdmin...
admin.site.register(UserProfile, UserAdmin)
# ... and, since we're not using Django's built-in permissions,
# unregister the Group model from admin.
admin.site.unregister(Group)

