from __future__ import unicode_literals
from register_page.admin import UserCreationForm
from register_page.models import *
from django.test import TestCase
from django import forms
from django.core.files import File
from django.contrib import auth

class LoginPageTests(TestCase):

    def setup(self):
        pass

    # Login page view loads
    def test_LoadLoginPage(self):
        response = self.client.get('http://127.0.0.1:8000/login/')

        self.assertEqual(response.status_code, 200)


    #Login Page has content
    def test_LoginPageContainsUsername(self):
        response = self.client.get('http://127.0.0.1:8000/login/')


        self.assertContains(response, "username")

    def test_PEPageContainsPassword(self):
        response = self.client.get('http://127.0.0.1:8000/login/')

        self.assertContains(response, "password")



    #Valid User Login
    def test_LoginPageValidUser(self):
        user = UserProfile.objects.create(username='testtest')
        user.set_password('12345')
        user.save()

        login = self.client.login(username='testtest', password='12345')
        self.assertTrue(login)

    # Invalid User Login
    def test_LoginPageInvalidUser(self):
        user = UserProfile.objects.create(username='testtest')
        user.set_password('12345')
        user.save()

        login = self.client.login(username='testtest', password='123456')
        self.assertFalse(login)

    #Login page redirect
    def test_LoginPageRedirect(self):
        user = UserProfile.objects.create(username='testtest')
        user.set_password('12345')
        user.save()

        response = self.client.post('http://127.0.0.1:8000/login/',{'username': 'testtest', 'password': '12345'}, follow=True)
        self.assertRedirects(response, 'http://127.0.0.1:8000/', 302)


    # valid user authenticated
    def test_AuthenticatedUser(self):
        user = UserProfile.objects.create(username='testtest')
        user.set_password('12345')
        user.save()

        self.client.post('http://127.0.0.1:8000/login/', {'username': 'testtest', 'password': '12345'},
                                    follow=True)

        user = auth.get_user(self.client)
        self.assertTrue( user.is_authenticated())

        # Invalid user authenticated

    def test_AuthenticatedUser(self):
        user = UserProfile.objects.create(username='testtest')
        user.set_password('12345')
        user.save()

        self.client.post('http://127.0.0.1:8000/login/', {'username': 'testtest', 'password': '12ff345'},
                         follow=True)

        user = auth.get_user(self.client)
        self.assertFalse(user.is_authenticated())

