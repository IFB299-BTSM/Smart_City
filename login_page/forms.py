from django import forms

from django.contrib.auth import authenticate, get_user_model




class UserLoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)


    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        user = authenticate(username = username, password = password)

        if not user:
            raise forms.ValidationError("User does not exist, make sure username and password are correct")

        return super(UserLoginForm, self).clean()
