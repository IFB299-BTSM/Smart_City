# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.views.generic import TemplateView
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import TemplateView
from django.contrib.auth.forms import UserCreationForm
from search_page.models import Place, Event

from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render


def item(request):
    title = request.GET.get('title', '')
    latitude = request.GET.get('latitude', '')
    longitude = request.GET.get('longitude', '')
    address = request.GET.get('address', '')
    type = request.GET.get('type', '')
    description = request.GET.get('description', '')
    picture = request.GET.get('picture', '')

    args = {"title": title, "address": address, "type": type, "description": description, "latitude": latitude,
            "longitude": longitude, "picture": picture}
    html = render(request, 'item_place_event_page.html', args)

    return HttpResponse(html)


def places(request):
    querysetPlace = Place.objects.all().order_by("type")
    querysetEvent = Event.objects.all()

    query = request.GET.get("q")
    if query:
        querysetPlace = querysetPlace.filter(
            Q(title__icontains=query) |
            Q(address__icontains=query) |
            Q(description__icontains=query) |
            Q(type__icontains=query)
        ).distinct()

        querysetEvent = querysetEvent.filter(
            Q(title__icontains=query) |
            Q(address__icontains=query) |
            Q(description__icontains=query) |
            Q(type__icontains=query)
        ).distinct()

    paginator = Paginator(querysetPlace, 10) # Show 10 places per page

    page = request.GET.get('page')
    try:
        querysetPlace = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        querysetPlace = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        querysetPlace = paginator.page(paginator.num_pages)

    context = {
        "events_list": querysetEvent,
        "places_list": querysetPlace,
        "title": "Search Results"

    }
    html = render(request, 'places.html', context)
    return HttpResponse(html)


def events(request):
    querysetPlace = Place.objects.all().order_by("type")
    querysetEvent = Event.objects.all()

    query = request.GET.get("q")
    if query:
        querysetPlace = querysetPlace.filter(

            Q(title__icontains=query) |
            Q(address__icontains=query) |
            Q(description__icontains=query) |
            Q(type__icontains=query)
        ).distinct()

        querysetEvent = querysetEvent.filter(
            Q(title__icontains=query) |
            Q(address__icontains=query) |
            Q(description__icontains=query) |
            Q(type__icontains=query)
        ).distinct()

    paginator = Paginator(querysetEvent, 10) # Show 10 events per page

    page = request.GET.get('page')
    try:
        querysetEvent = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        querysetEvent = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        querysetEvent = paginator.page(paginator.num_pages)
        
    context = {
        "events_list": querysetEvent,
        "places_list": querysetPlace,
        "title": "Search Results"

    }
    html = render(request, 'events.html', context)
    return HttpResponse(html)
