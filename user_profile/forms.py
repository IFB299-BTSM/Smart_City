from register_page.models import UserProfile
from register_page.admin import UserChangeForm
import re
from register_page.models import UserProfile
from django.utils.safestring import mark_safe
from django import forms



class UserForm(UserChangeForm):


    class Meta:
        model = UserProfile
        fields = ('email', 'type','given_names', 'family_name', 'password', 'profile_picture')




    def save(self, commit=True):
        user = super(UserForm, self).save(commit=False)
        if commit:
            user.save()
        return user
