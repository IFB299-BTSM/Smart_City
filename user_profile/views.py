# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.contrib.auth.forms import UserCreationForm
from register_page.admin import UserChangeForm
from django.http import HttpResponse
from user_profile.forms import UserForm
from user_profile.admin import PasswordChangeForm

# Create your views here.


def display_profile(request):
    args = {'user': request.user}
    return render(request, 'profile.html', args)




def edit_profile(request):

    print("Hello")

    if request.method == 'POST':
        form = UserForm(request.POST, instance=request.user)

        if form.is_valid():

            form.save()

            return redirect('/')

    else:
        form = UserForm(instance=request.user)

    args = {'form': form}
    html = render(request, 'edit_profile.html', args)
    return HttpResponse(html)

def change_password(request):
    if request.user.is_authenticated:

        if request.method == 'POST':
            form = PasswordChangeForm(data=request.POST, user=request.user)

            if form.is_valid():

                form.save()

                return redirect('/')

        else:
            form = PasswordChangeForm(user=request.user)

        args = {'form': form}

        html = render(request, 'change_password.html', args)
    else:
        html = render(request, 'change_password.html')

    return HttpResponse(html)

