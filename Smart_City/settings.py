import os

#MEDIA_URL = '/Smart_City/media/'
#MEDIA_ROOT = os.path.join(BASE_DIR, 'Smart_City/media')


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '=a*l5cufxh)p*a-(4me+bvgri)9r9191r_=c!$!&nr@j1!r-e&'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'djangocms_admin_style',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'home_page',
    'register_page',
    'easy_thumbnails',
    'sitetree',
    'search_page',
    'item_place_event_page',
    'cms',
    'treebeard',
    'menus',
    'sekizai',
    'filer',
    'mptt',
    'djangocms_text_ckeditor',
    'olefile',
    'PIL',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.middleware.locale.LocaleMiddleware'
   # 'cms.middleware.user.CurrentUserMiddleware',
   # 'cms.middleware.page.CurrentPageMiddleware',
   # 'cms.middleware.toolbar.ToolbarMiddleware',
   # 'cms.middleware.language.LanguageCookieMiddleware',

)

THUMBNAIL_HIGH_RESOLUTION = True

THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters'
)


ROOT_URLCONF = 'Smart_City.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'sekizai.context_processors.sekizai',
                'cms.context_processors.cms_settings',
                'search_page.context_processors.add_variable_to_context',
            ],
        },
    },
]


CMS_TEMPLATES = [
    ('template_1.html',         'Empty Template 1'),
    ('home_page.html',          'Home Page App'),
    ('edit_profile.html',       'Edit Profile App'),
    ('contact.html',            'Contact Page'),
    ('register_page.html',      'Register App'),
    ('search_page.html',        'Search Page'),
    ('about.html',              'About Page'),
    ('login_page.html',         'Login Page'),
    ('places.html',             'Places App'),
    ('events.html',             'Events App'),
]


SOUTH_MIGRATION_MODULES = {
'sitetree': 'sitetree.south_migrations',
}

# Define template directory
from os.path import abspath, basename, dirname, join, normpath

DJANGO_ROOT = dirname(dirname(abspath(__file__)))

SITE_ROOT = dirname(DJANGO_ROOT)

SITE_ID = 1

WSGI_APPLICATION = 'Smart_City.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'en-us'

LANGUAGES = [
  ('en-us', ('English')),
]

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATIC_URL = '/static/'

MEDIA_URL = '/Smart_City/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'Smart_City/media')

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
    '/static/',
    '/templates/',
]

PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    #'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
]

AUTH_PROFILE_MODULE = 'register_page.UserProfile'

AUTHENTICATION_BACKENDS = ('django.contrib.auth.backends.ModelBackend','login_page.custom_backend.CustomBackend',)

SESSION_COOKIE_AGE = 10000
SESSION_SAVE_EVERY_REQUEST = True

CKEDITOR_SETTINGS = {
    'stylesSet': 'default:/static/js/addons/ckeditor.wysiwyg.js',
    'contentsCss': ['/static/css/style.css'],
    'contentsCss': ['/static/css/lightbox.css'],
    'contentsCss': ['/static/css/menu.css'],
    'contentsCss': ['/static/css/zerogrid.css'],
    'contentsCss': ['/static/css/zerostyle.css'],
    'contentsCss': ['/static/font-awesome/css/font-awesome.min.css'],

}


