"""helloworld_project URL Configuration
The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import include, url
from django.contrib import admin
from home_page.views import HomeView
from login_page.views import login_user, logout_user
from register_page.views import register
from user_profile.views import display_profile, edit_profile, change_password
from item_place_event_page.views import places, events, item
from create_event.views import create_event




urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', HomeView.as_view()),
    url(r'^login/$', login_user, name='login_user'),
    url(r'^register/$', register, name='register'),
    url(r'^search/', include("search_page.urls"), name='search_page'),
    url(r'^logout/', logout_user, name='logout_user'),
    url(r'^profile/$', display_profile, name='display_profile'),
    url(r'^profile/edit/$', edit_profile, name='edit_profile'),
    url(r'^profile/edit/change_password/$', change_password, name='change_password'),
    url(r'^places/$', places, name='places'),
    url(r'^events/$', events, name='events'),
    url(r'^search/item$', item, name='item'),
    url(r'^newevent$', create_event, name='create event'),
    url(r'^', include('cms.urls')),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

